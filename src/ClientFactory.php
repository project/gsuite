<?php

namespace Drupal\gsuite;

use Drupal\Core\Site\Settings;
use Google_Client;

/**
 * Google Client Factory class.
 */
class ClientFactory {

  /**
   * The settings service.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * ClientFactory constructor.
   *
   * @param \Drupal\Core\Site\Settings $settings
   *   Settings object.
   */
  public function __construct(Settings $settings) {
    $this->settings = $settings;
  }

  /**
   * Get the Google client.
   *
   * @return \Google_Client
   *   Google client.
   *
   * @throws \Google_Exception
   */
  public function getClient() {
    $client = new Google_Client();

    $client->setAuthConfig($this->getSetting('auth_config'));

    $client->setAccessType('offline');

    $client->setPrompt('select_account consent');

    return $client;
  }

  /**
   * Get setting using key from settings variable.
   *
   * @param string $key
   *   The key of the setting defined under 'gsuite' parent array.
   *
   * @return bool|string
   *   Either the value of false.
   */
  protected function getSetting(string $key) {
    $settings = $this->settings->get('gsuite');
    if (array_key_exists($key, $settings) == FALSE) {
      return FALSE;
    }

    return $settings[$key];
  }

}
